import 'package:app_francesinha/public/features/application/app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('pt', 'PT'),
      ],
      path: 'assets/translations',
      fallbackLocale: const Locale('pt', 'PT'),
      child: const RestaurantApp(),
    ),
  );
}
