import 'package:app_francesinha/public/features/application/navigation_extensions.dart';
import 'package:app_francesinha/public/features/detail/application/selected_food_inherited_widget.dart';
import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:flutter/material.dart';

class ListMenuItemWidget extends StatelessWidget {
  const ListMenuItemWidget({super.key, required this.foodItem});

  final FoodItem foodItem;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Card(
        color: Colors.white,
        margin: const EdgeInsets.symmetric(horizontal: 16),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: InkWell(
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          onTap: () {
            /// Save selected food to state
            final store = SelectedFoodInheritedWidget.of(context);
            final selected = store.selected;
            selected.setFood(foodItem);

            // Navigate to detail page
            context.navigateToDetailPage();
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: SizedBox(
                  height: 120,
                  width: 120,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(21)),
                    child: foodItem.image.isNotEmpty
                        ? Image.asset(foodItem.image)
                        : const Placeholder(),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      foodItem.title,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 8),
                    Text(
                      foodItem.subtitle,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  foodItem.price.toString(),
                  style: Theme.of(context).textTheme.displayMedium?.apply(
                        color: Theme.of(context).primaryColor,
                      ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
