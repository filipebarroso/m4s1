import 'package:app_francesinha/public/features/application/app_theme.dart';
import 'package:app_francesinha/public/features/application/navigations.dart';
import 'package:app_francesinha/public/features/detail/application/selected_food_inherited_widget.dart';
import 'package:app_francesinha/public/features/detail/presentation/detail_simple_page.dart';
import 'package:app_francesinha/public/features/home/application/food_service.dart';
import 'package:app_francesinha/public/features/home/data/home_repository.dart';
import 'package:app_francesinha/public/features/home/presentation/pages/home_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class RestaurantApp extends StatelessWidget {
  const RestaurantApp({super.key});

  @override
  Widget build(BuildContext context) {
    return SelectedFoodInheritedWidget(
      selected: SelectedFood.nonePicked(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'app_name'.tr(),
        // Theme
        theme: AppTheme().themeData,
        // Localization
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        // Routes & Navigation
        initialRoute: AppNavigationPaths.home,
        routes: {
          AppNavigationPaths.home: (context) => HomePage(
                homeRepository: HomeRepository(FoodService()),
              ),
          AppNavigationPaths.detail: (context) => const DetailSimplePage(),
        },
        onGenerateRoute: (settings) {
          /// This is a good place to handle named routes and pass arguments
          /// Example:
          /// Assume you need to pass an obj NavigatorParameters to the DetailSimplePage
          // if (settings.name == NavsNames.detail) {
          //   if (settings.arguments is NavigatorParameters) {
          //     final arg = settings.arguments as NavigatorParameters;

          //     return MaterialPageRoute(
          //       builder: (context) {
          //         return DetailSimplePage(arg.data);
          //       },
          //     );
          //   }
          // }

          assert(false, 'Need to implement ${settings.name}');
          return null;
        },
      ),
    );
  }
}
