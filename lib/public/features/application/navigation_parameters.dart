/// Helper class to pass data between screens
class AppNavigationParameters<T> {
  final T data;

  AppNavigationParameters(this.data);
}
