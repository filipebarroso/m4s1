// All possible paths to navigate to.
class AppNavigationPaths {
  static const String home = '/';
  static const String detail = '/detail';
}
