import 'package:flutter/material.dart';

class AppTheme {
  final ThemeData _themeData;

  // Still using Material 2
  AppTheme()
      : _themeData = ThemeData(
          primarySwatch: Colors.red,
          textTheme: const TextTheme(
            titleLarge: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            titleMedium: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
              fontStyle: FontStyle.italic,
              color: Colors.grey,
            ),
            displayMedium: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          cardTheme: const CardTheme(
            color: Colors.red,
          ),
        );

  ThemeData get themeData => _themeData;
}
