import 'dart:convert';

List<FoodItemEntity> francesinhasFromJson(String str) =>
    List<FoodItemEntity>.from(
      json.decode(str).map(
            (x) => FoodItemEntity.fromJson(x),
          ),
    );

String francesinhasToJson(List<FoodItemEntity> data) => json.encode(
      List<dynamic>.from(
        data.map(
          (x) => x.toJson(),
        ),
      ),
    );

class FoodItemEntity {
  final int id;
  final String name;
  final String description;
  final int price;

  FoodItemEntity({
    required this.id,
    required this.name,
    required this.description,
    required this.price,
  });

  factory FoodItemEntity.fromRawJson(String str) =>
      FoodItemEntity.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FoodItemEntity.fromJson(Map<String, dynamic> json) => FoodItemEntity(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "price": price,
      };
}
