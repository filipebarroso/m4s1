import 'package:app_francesinha/public/features/detail/application/selected_food_inherited_widget.dart';
import 'package:flutter/material.dart';

class DetailSimplePage extends StatelessWidget {
  const DetailSimplePage({super.key});

  @override
  Widget build(BuildContext context) {
    final inherited = SelectedFoodInheritedWidget.of(context);
    final foodItem = inherited.selected.food;
    return Scaffold(
      appBar: AppBar(),
      body: SizedBox.expand(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 200,
              child: foodItem?.image != null
                  ? Image.asset(foodItem!.image)
                  : const Placeholder(),
            ),
            Text(
              foodItem?.title ?? 'No food selected',
              style: Theme.of(context).textTheme.displayMedium,
            ),
            Text(
              foodItem?.subtitle ?? 'No food selected',
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
      ),
    );
  }
}
